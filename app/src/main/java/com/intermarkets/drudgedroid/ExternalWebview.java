package com.intermarkets.drudgedroid;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.openx.view.plugplay.errors.AdError;
import com.openx.view.plugplay.listeners.AdEventsListener;
import com.openx.view.plugplay.models.AdConfiguration;
import com.openx.view.plugplay.networking.parameters.UserParameters;
import com.openx.view.plugplay.views.AdView;


public class ExternalWebview extends Activity {

    public static final String TAG = ExternalWebview.class.getSimpleName();

    private int currentApiVersion;

    public static WebView extView;

    public Boolean doOnce = true;

    Intent launchBrowser;

    WebSettings webSettings;

    private AdView m_adView;
    private boolean m_showInterstitial;

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        // This work only for android 4.4+
        if(currentApiVersion >= Build.VERSION_CODES.KITKAT)
        {

            getWindow().getDecorView().setSystemUiVisibility(flags);

            // Code below is to handle presses of Volume up or Volume down.
            // Without this, after pressing volume buttons, the navigation bar will
            // show up and won't hide
            final View decorView = getWindow().getDecorView();
            decorView
                    .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
                    {

                        @Override
                        public void onSystemUiVisibilityChange(int visibility)
                        {
                            if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                            {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        String newurl = intent.getStringExtra("newurl");

        currentApiVersion = android.os.Build.VERSION.SDK_INT;


        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        // This work only for android 4.4+
        if(currentApiVersion >= Build.VERSION_CODES.KITKAT)
        {

            getWindow().getDecorView().setSystemUiVisibility(flags);

            // Code below is to handle presses of Volume up or Volume down.
            // Without this, after pressing volume buttons, the navigation bar will
            // show up and won't hide
            final View decorView = getWindow().getDecorView();
            decorView
                    .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
                    {

                        @Override
                        public void onSystemUiVisibilityChange(int visibility)
                        {
                            if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                            {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
        }

        setContentView(R.layout.activity_external_webview);


        extView = (WebView) findViewById(R.id.extView);

        try {
            extView.loadUrl(newurl);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


        final ProgressBar Pbar;
        Pbar = (ProgressBar) findViewById(R.id.pBB1);
        extView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100 && Pbar.getVisibility() == ProgressBar.GONE) {
                    Pbar.setVisibility(ProgressBar.VISIBLE);

                }
                Pbar.setProgress(progress);
                if (progress >= 70) {
                    Pbar.setVisibility(ProgressBar.GONE);
                    extView.setVisibility(View.VISIBLE);
                }
            }
        });


        extView.setVisibility(View.GONE);

        extView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadUrl(url);

                return true;

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                extView.setVisibility(View.VISIBLE);
            }

        });

        webSettings = extView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        //webSettings.setDomStorageEnabled(true);
       // webSettings.setBuiltInZoomControls(true);
        //webSettings.setSupportZoom(true);

        //webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);

        extView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);


        final ImageButton backbutton = (ImageButton) findViewById(R.id.backExtButton);
        backbutton.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (extView.canGoBack()) {
                        extView.goBack();
                    } else {
                        //  Pbar.setVisibility(ProgressBar.VISIBLE);
                        extView.setVisibility(View.GONE);


                        finish();

                        MainActivity.WriteHTML(300);

                    }


                    backbutton.setBackgroundResource(R.drawable.ic_back_onclick);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    backbutton.setBackgroundResource(R.drawable.ic_back);
                }
                return false;
            }


        });

        final ImageButton homebutton = (ImageButton) findViewById(R.id.homeExtButton);
        homebutton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    // contentView.setVisibility(View.GONE);
                    // drudgeView.setVisibility(View.VISIBLE);

                    extView.setVisibility(View.GONE);

                    finish();

                    MainActivity.WriteHTML(300);

                    homebutton.setBackgroundResource(R.drawable.ic_home_onclick);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    homebutton.setBackgroundResource(R.drawable.ic_home);
                }
                return false;
            }
        });



        final ImageButton extbutton = (ImageButton) findViewById(R.id.extExtButton);
        extbutton.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    String homeurl = extView.getUrl();
                   // System.out.println(homeurl);
                    // only if not on home page


                   // if (!homeurl.contains("file://")) {
                        launchBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(homeurl));
                        startActivity(launchBrowser);
                    //} else {
                    //    launchBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.drudgereport.com"));
                    //    startActivity(launchBrowser);
                    //}

                    extbutton.setBackgroundResource(R.drawable.ic_open_onclick);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    extbutton.setBackgroundResource(R.drawable.ic_openout);
                }
                return false;
            }


        });

        final ImageButton refreshbutton = (ImageButton) findViewById(R.id.refreshExtButton);
        refreshbutton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    extView.reload();
                    refreshbutton.setBackgroundResource(R.drawable.ic_refresh_onclick);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    refreshbutton.setBackgroundResource(R.drawable.ic_refresh);
                }
                return false;
            }
        });

        // Request an ad
        loadBannerAd();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadBannerAd() {
        String adUnitId = null;
        if (MainActivity.isTablet(this)) {
            adUnitId = getResources().getString(R.string.simple_banner_adunit_id_tablet);
        }
        else {
            adUnitId = getResources().getString(R.string.simple_banner_adunit_id_mobile);
        }

        // Instantiate an ad view
        try {
            m_adView = new AdView(this,
                                  getResources().getString(R.string.ad_domain),
                                  adUnitId,
                                  AdConfiguration.AdUnitIdentifierType.AUID);
        }
        catch (Exception e) {
            Log.e(TAG, "AdView creation failed");
            return;
        }

        // Add ad view to ad container
        RelativeLayout lytBannerAd = (RelativeLayout) findViewById(R.id.lytBannerAd);
        lytBannerAd.addView(m_adView);

        // Set whether to show ad immediately after load
        m_adView.setAutoDisplayOnLoad(true);

        // Set listener for ad life cycle events
        m_adView.addAdEventListener(bannerAdEventListener);

        // Set time interval to refresh the ad
        m_adView.setAutoRefreshDelay(30);

        // Set user targeting parameters
        UserParameters userParams = new UserParameters();
        userParams.setCity("LosAngeles");
        m_adView.setUserParameters(userParams);

        // Request an ad
        m_adView.load();
    }

    private AdEventsListener bannerAdEventListener = new AdEventsListener() {
        /*
        * Fires when an ad loads successfully
        */
        @Override
        public void adDidLoad(AdView adView) {
            Log.i(TAG, "adDidLoad");
        }

        /*
         * Fires when an ad fails to load
         */
        @Override
        public void adDidFailToLoad(AdView adView, AdError adError) {
            Log.i(TAG, "adDidFailToLoad");
        }

        /*
         * Fires when an ad is displayed
         */
        @Override
        public void adDidDisplay(AdView adView) {
            Log.i(TAG, "adDidDisplay");
        }

        /*
         * Fires when an ad completes
         */
        @Override
        public void adDidComplete(AdView adView) {
            Log.i(TAG, "adDidComplete");
        }

        /*
         * Fires when an ad is clicked
         */
        @Override
        public void adWasClicked(AdView adView) {
            Log.i(TAG, "adWasClicked");
        }

        /*
         * Fires when a click through view closes
         */
        @Override
        public void adClickThroughDidClose(AdView adView) {
            Log.i(TAG, "adClickThroughDidClose");
        }

        /*
         * Fires when an interstitial ad closes
         */
        @Override
        public void adInterstitialDidClose(AdView adView) {
            Log.i(TAG, "adInterstitialDidClose");
        }

        /*
         * Fires when an mraid expand is clicked
         */
        @Override
        public void adDidExpand(AdView adView) {
            Log.i(TAG, "adDidExpand");
        }

        /*
         * Fires when an mraid collapse is clicked
         */
        @Override
        public void adDidCollapse(AdView adView) {
            Log.i(TAG, "adDidCollapse");
        }
    };
}
