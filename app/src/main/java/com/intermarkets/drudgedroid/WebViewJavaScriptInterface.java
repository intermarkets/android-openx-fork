package com.intermarkets.drudgedroid;

import android.content.Context;
import android.location.Location;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import org.jsoup.helper.StringUtil;

import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Deeeev on 08/07/2015.
 */
public class WebViewJavaScriptInterface{

    private Context context;

    /*
     * Need a reference to the context in order to sent a post message
     */
    public WebViewJavaScriptInterface(Context context){
        this.context = context;
    }


    @JavascriptInterface
    public void ReWrite(){
        MainActivity.WriteHTML(0);
    }

    @JavascriptInterface
    public void ReadyForJSON(){
        MainActivity.FireJS();
    }

    /*
     * This method can be called from Android. @JavascriptInterface
     * required after SDK version 17.
     */
    @JavascriptInterface
    public void makeToast(String message, boolean lengthLong){
        Toast.makeText(context, message, (lengthLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT)).show();
    }

    @JavascriptInterface
    public String getEnrichedAdReqUrl(String domain, String adUnitId) {
        String adReqUrl = "http://" + domain + "/ma/1.0/arh?auid=" + adUnitId;
        LinkedHashMap<String, String> queryArgs = new LinkedHashMap<>();

        // Advertising data
        MainActivity.AdvertisingData advertisingData = getAdvertisingData();
        if (advertisingData != null) {
            queryArgs.put("did.adid", advertisingData.AdvertisingId);
            queryArgs.put("did.adid.enabled", String.valueOf(advertisingData.IsLimitAdTrackingEnabled));
        }

        // Location data
        Location location = getLocationData();
        if (location != null) {
            queryArgs.put("lat", String.valueOf(location.getLatitude()));
            queryArgs.put("lon", String.valueOf(location.getLongitude()));
            queryArgs.put("lt", String.valueOf(1));
        }

        // Bundle id
        String bundleId = getBundleId();
        if (!StringUtil.isBlank(bundleId)) {
            queryArgs.put("app.bundle", bundleId);
        }

        for (Map.Entry<String, String> arg : queryArgs.entrySet()) {
            try {
                adReqUrl += "&" + arg.getKey() + "=" + URLEncoder.encode(arg.getValue(), "UTF-8");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        return adReqUrl;
    }

    private MainActivity.AdvertisingData getAdvertisingData() {
        MainActivity.AdvertisingData data = ((MainActivity) context).getAdvertisingData();
        return data;
    }

    private Location getLocationData() {
        Location location = ((MainActivity) context).getLastLocation();
        return location;
    }

    private String getBundleId() {
        String bundleId = context.getPackageName();
        return bundleId;
    }
}