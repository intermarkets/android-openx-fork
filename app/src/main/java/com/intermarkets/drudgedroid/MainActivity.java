package com.intermarkets.drudgedroid;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.openx.view.plugplay.errors.AdError;
import com.openx.view.plugplay.interstitial.InterstitialManager;
import com.openx.view.plugplay.interstitial.InterstitialVideoProperties;
import com.openx.view.plugplay.listeners.AdEventsListener;
import com.openx.view.plugplay.models.AdConfiguration;
import com.openx.view.plugplay.networking.parameters.UserParameters;
import com.openx.view.plugplay.video.VideoProperties;
import com.openx.view.plugplay.views.AdView;
import com.quantcast.measurement.service.QuantcastClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


public class MainActivity extends Activity {

    public static final String TAG = MainActivity.class.getSimpleName();

    private int currentApiVersion;

    public static WebView drudgeView;
    //public static WebView contentView;
    Intent launchBrowser;


    public GoogleAnalytics analytics;
    public Tracker tracker;
    WebSettings webSettings;
    public Context drudgeViewContext;
   // WebSettings contentWebSettings;

    AlertDialog alert;

    public static String sTopHeadlines= "";
    public static String sTopCenterHeadline= "";
    public static String sDrudgeSiteLogo= "";
    public static String sHeadlinesColumnOne= "";
    public static String sHeadlinesColumnTwo= "";
    public static String sHeadlinesColumnThree= "";
    public static String sSourcesColumnOne= "";
    public static String sSourcesColumnTwo= "";
    public static String sSourcesColumnThree= "";



    public static String s = "";
    public static String contents;

    public static String st;

    static String allhtml, temp, substr;
    static int a, b;

  //  public boolean clearHistory = false;
//
  //  public String landingURL = "";
  //  public boolean firstLoad = false;

    public static ProgressDialog progress;

    public static boolean isWriteDone = false;

    private String mExternalUrl;

    // private AdView m_bannerAdView;
    // private String m_bannerAdHtml;
    private AdView mDisplayAdView;
    private boolean mDisplayAdReady;
    private AdView mVideoAdView;
    private boolean mVideoAdReady;
    private int mInterstitialCount = 0;
    private int INTERSTITIAL_INTERVAL = 2;

    private Handler mHandler;
    private GoogleApiClient mGoogleApiClient;
    private String SHARED_PREF_NAME = "DRUDGE_DATA";
    private String PREF_ADVERTISING_ID = "ADVERTISING_ID";
    private String PREF_LIMIT_AD_TRACKING_ENABLED = "PREF_LIMIT_AD_TRACKING_ENABLED";

    public static class AdvertisingData {
        String AdvertisingId;
        boolean IsLimitAdTrackingEnabled;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState
    ) {

        super.onCreate(savedInstanceState);

        mHandler = new Handler();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(1800);

        tracker = analytics.newTracker("UA-451855-9"); // Replace with actual tracker/property Id
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);
        tracker.setScreenName("Home Page");
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("Track")
                .setAction("New")
                .setLabel("Homepage")
                .build());


        QuantcastClient.activityStart(this, "0nq5uo7apf52qbz4-d1huq39a6s5dfrt1", null, null);

        String additionalLabel = "Home Page";
        String theEventStr = "New User";
        QuantcastClient.logEvent(theEventStr, additionalLabel);



        currentApiVersion = android.os.Build.VERSION.SDK_INT;

        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        // This work only for android 4.4+
        if(currentApiVersion >= Build.VERSION_CODES.KITKAT)
        {

            getWindow().getDecorView().setSystemUiVisibility(flags);

            // Code below is to handle presses of Volume up or Volume down.
            // Without this, after pressing volume buttons, the navigation bar will
            // show up and won't hide
            final View decorView = getWindow().getDecorView();
            decorView
                    .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
                    {

                        @Override
                        public void onSystemUiVisibilityChange(int visibility)
                        {
                            if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                            {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
        }


        setContentView(R.layout.activity_main);


      //  contentView = (WebView) findViewById(R.id.drudgeViewContent);
        drudgeView = (WebView) findViewById(R.id.drudgeView);
        drudgeView.loadUrl("file:///android_asset/index.html");
       // contentView.setVisibility(View.GONE);

        drudgeViewContext = drudgeView.getContext();

        final ProgressBar Pbar;
        Pbar = (ProgressBar) findViewById(R.id.pB1);
        drudgeView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100 && Pbar.getVisibility() == ProgressBar.GONE) {
                    Pbar.setVisibility(ProgressBar.VISIBLE);

                }
                Pbar.setProgress(progress);
                if (progress == 100) {
                    Pbar.setVisibility(ProgressBar.GONE);

                }
            }
        });


        drudgeView.setWebViewClient(new WebViewClient() {

           // @Override
           // public void onPageFinished(WebView view, String url) {
           //     if(!isTablet(getApplicationContext())) {
           //         drudgeView.loadUrl("javascript:RemoveTopAd()");
           //         Log.d("DRUDGE", "Mobile so removing top ad");
           //     } else {
           //         Log.d("DRUDGE", "Tablet - Keep top ad");
           //     }
           //     super.onPageFinished(view, url);
           // }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {



                if (!url.equalsIgnoreCase("file:///#")) {
                    // Intent intent = new Intent(drudgeViewContext, ExternalWebview.class);
                    // intent.putExtra("newurl", url);
                    // startActivity(intent);

                    mExternalUrl = url;

                    // TODO: Uncomment when interstitial ad unit id / tag is set in strings.xml
                    // mInterstitialCount++;
                    // if (mInterstitialCount > INTERSTITIAL_INTERVAL) {
                    //     mInterstitialCount = 0;
                    //
                    //     showInterstialAd();
                    // }
                    // else {
                        startExternalWebView();
                    // }
                    // TODO: End

                    // WriteHTML();
                } else {
                    //view.reload();
                    //  return false;
                }


                return true;

            }
        });





        /*
        contentView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                contentView.setVisibility(View.VISIBLE);
                if (firstLoad) {
                    landingURL = contentView.getUrl();

                    landingURL = landingURL.replaceAll("/", "");
                    firstLoad = false;
                    //    Log.d("DRUDGE", "landingURL" + landingURL);
                    //    Log.d("DRUDGE", "origURL" + contentView.getUrl());

                }

                if (clearHistory) {
                    contentView.clearHistory();

                    //  Log.d("DRUDGE", "clearHistory now");
                    clearHistory = false;
                }
                super.onPageFinished(view, url);
            }
        });
*/

       // contentWebSettings = contentView.getSettings();
       // contentWebSettings.setJavaScriptEnabled(true);


        webSettings = drudgeView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);

        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);

        drudgeView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        drudgeView.addJavascriptInterface(new WebViewJavaScriptInterface(this), "drudgeapp");

        if(android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            fixJellyBeanIssues();
        }




        final ImageButton backbutton = (ImageButton) findViewById(R.id.backButton);
        backbutton.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if(drudgeView.canGoBack() ) {
                        drudgeView.goBack();
                    }

                    /*
                    Log.d("DRUDGE", "landingURL" + landingURL);
                    Log.d("DRUDGE", "currentURL" + contentView.getUrl());

                    String compareURL = contentView.getUrl();
                    compareURL = compareURL.replaceAll("/", "");
                    Log.d("DRUDGE", "compareURL" + compareURL);
                    if(!contentView.canGoBack() ) {

                        contentView.setVisibility(View.GONE);
                        drudgeView.setVisibility(View.VISIBLE);
                        //drudgeView.reload();
                    } else {
                        contentView.goBack();
                    }*/
                    /*
                   if(contentView.canGoBack()) {
                       contentView.goBack();
                   } else {

                       contentView.setVisibility(View.GONE);
                       drudgeView.setVisibility(View.VISIBLE);

                     //  contentView.clearCache(false);
                     //  contentView.clearHistory();

                   }*/




                    backbutton.setBackgroundResource(R.drawable.ic_back_onclick);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    backbutton.setBackgroundResource(R.drawable.ic_back);
                }
                return false;
            }


        });

        /*
        final ImageButton homebutton = (ImageButton) findViewById(R.id.homeButton);
        homebutton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    drudgeView.loadUrl("file:///android_asset/index.html");
                   // contentView.setVisibility(View.GONE);
                   // drudgeView.setVisibility(View.VISIBLE);

                    homebutton.setBackgroundResource(R.drawable.ic_home_onclick);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    homebutton.setBackgroundResource(R.drawable.ic_home);
                }
                return false;
            }
        });
*/

        final ImageButton extbutton = (ImageButton) findViewById(R.id.extButton);
        extbutton.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    //String homeurl = drudgeView.getUrl();
                   // System.out.println(homeurl);
                    // only if not on home page


                   // if (!homeurl.contains("file://")) {
                   //     launchBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(homeurl));
                   //     startActivity(launchBrowser);
                   // } else {
                        launchBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.drudgereport.com"));
                        startActivity(launchBrowser);
                   // }
                    extbutton.setBackgroundResource(R.drawable.ic_open_onclick);


                } else if (event.getAction() == MotionEvent.ACTION_UP) {


                    extbutton.setBackgroundResource(R.drawable.ic_openout);
                }
                return false;
            }


        });

        final ImageButton refreshbutton = (ImageButton) findViewById(R.id.refreshButton);
        refreshbutton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    drudgeView.reload();
                    reloadOpenXJsAds();
                    refreshbutton.setBackgroundResource(R.drawable.ic_refresh_onclick);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    refreshbutton.setBackgroundResource(R.drawable.ic_refresh);
                }
                return false;
            }
        });

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {

                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {

                        }
                    })
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // TODO: Uncomment when interstitial display ad unit id is set in strings.xml
        // mHandler.removeCallbacks(runLoadDisplayAd);

        // TODO: Uncomment when interstitial video tag is set in strings.xml
        // mHandler.removeCallbacks(runLoadVideoAd);
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            reloadOpenXJsAds();

            // TODO: Uncomment when interstitial display ad unit id is set in strings.xml
            // loadDisplayAd();

            // TODO: Uncomment when interstitial video tag is set in strings.xml
            // loadVideoAd();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }

    public static void WriteHTML(int delay) {


        progress = new ProgressDialog(drudgeView.getContext());
        progress.setTitle("Drudge Report");
        progress.setMessage("Refreshing Links...");
        progress.show();

// To dismiss the dialog



     //   ProgressDialog.show(drudgeView.getContext(), "Loading", "Wait while loading...");

        drudgeView.postDelayed(new Runnable() {
            @Override
            public void run() {

                // drudgeView.setVisibility(View.GONE);

                // android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

                //org.jsoup.nodes.Document html = Jsoup.parse(s);

               // Log.d("DRUDGE: ", s.toString());

               // Log.d("DRUDGE: ", sTopHeadlines.toString());



                sTopHeadlines = sTopHeadlines.replace("\"", "'");

                //Log.d("DRUDGE: ", sTopHeadlines.toString());

                drudgeView.loadUrl("javascript:WriteTopHeadline(\"" + sTopHeadlines + "\");");

                sTopCenterHeadline = sTopCenterHeadline.replace("\"", "'");

                sDrudgeSiteLogo = sDrudgeSiteLogo.replace("\"", "'");

                drudgeView.loadUrl("javascript:WriteHeadline(\"" + sTopCenterHeadline + sDrudgeSiteLogo + "\");");



                sHeadlinesColumnOne = sHeadlinesColumnOne.replace("\"", "'");

                drudgeView.loadUrl("javascript:WriteLeftCol(\"" + sHeadlinesColumnOne + "\");");


                sSourcesColumnOne = sSourcesColumnOne.replace("\"", "'");

                drudgeView.loadUrl("javascript:WriteLeftColLower(\"" + sSourcesColumnOne + "\");");





                sHeadlinesColumnTwo = sHeadlinesColumnTwo.replace("\"", "'");

                drudgeView.loadUrl("javascript:WriteMiddleCol(\"" + sHeadlinesColumnTwo + "\");");


                sSourcesColumnTwo = sSourcesColumnTwo.replace("\"", "'");

                drudgeView.loadUrl("javascript:WriteMiddleColBot(\"" + sSourcesColumnTwo + "\");");



                sHeadlinesColumnThree = sHeadlinesColumnThree.replace("\"", "'");

                drudgeView.loadUrl("javascript:WriteRightTopCol(\"" + sHeadlinesColumnThree + "\");");


                sSourcesColumnThree = sSourcesColumnThree.replace("\"", "'");

                drudgeView.loadUrl("javascript:WriteRightBotCol(\"" + sSourcesColumnThree + "\");");




                /*

                org.jsoup.nodes.Document html = Jsoup.parse(contents);

                org.jsoup.nodes.Element ele = html.getElementById("drudgeTopHeadlines");

                st = ele.html();

                st = st.replace("\"", "'");

                drudgeView.loadUrl("javascript:WriteHeadline(\"" + st + "\");");


                allhtml = html.toString();


                a = allhtml.indexOf("FIRST COLUMN STARTS HERE");
                a += 27;
                b = allhtml.indexOf("JavaScript Tag  // Website: DrudgeReport");
                b -= 4;

                temp = allhtml.substring(a, b);
                temp = temp.replace("\"", "'");

                drudgeView.loadUrl("javascript:WriteLeftCol(\"" + temp + "\");");
                ////

                a = allhtml.indexOf("L I N K S    F I R S T    C O L U M N");
                a += 41;
                b = allhtml.indexOf("<div style=\"width:1px;background-color:#C0C0C0;margin-left:1px;margin-right:1px;height:2500px;\"></div>");
                b -= 100;

                temp = allhtml.substring(a, b);
                temp = temp.replace("\"", "'");
                drudgeView.loadUrl("javascript:WriteLeftColLower(\"" + temp + "\");");

                ///

                substr = allhtml.substring(b + 207, allhtml.length() - 1);

                //a = allhtml.indexOf("SECOND COLUMN BEGINS HERE");
                // a += 26;
                b = substr.indexOf("<div style=\"width:1px;background-color:#C0C0C0;margin-left:1px;margin-right:1px;height:2500px;\"></div>");
                //   b -= 25;

                temp = substr.substring(3, b - 53);
                temp = temp.replace("\"", "'");
                drudgeView.loadUrl("javascript:WriteMiddleCol(\"" + temp + "\");");


                substr = substr.substring(b, substr.length() - 1);


                b = substr.indexOf("<div id=\"1131611\" align=\"left\" style=\"width:300px;padding:0px;margin:0px;overflow:visible;text-align:left\">");

                temp = substr.substring(110, b);
                temp = temp.replace("\"", "'");
                drudgeView.loadUrl("javascript:WriteRightTopCol(\"" + temp + "\");");


                substr = substr.substring(b, substr.length() - 1);

                a = substr.indexOf("L I N K S    A N D   S E A R C H E S     3 R D    C O L U M N");
                b = substr.indexOf("<!--JavaScript Tag  // Website: DrudgeReport // ");


                temp = substr.substring(a + 64, b);
                temp = temp.replace("\"", "'");


                drudgeView.loadUrl("javascript:WriteRightMidCol(\"" + temp + "\");");


                substr = substr.substring(b, substr.length() - 1);

                a = substr.indexOf("<!-- End of JavaScript Tag -->");
                b = substr.indexOf("<!-- Page Reloader, Headline Updater, eProof, DRAMini -->");


                temp = substr.substring(a, b);
                temp = temp.replace("\"", "'");


                drudgeView.loadUrl("javascript:WriteRightBotCol(\"" + temp + "\");");

                */

              //   drudgeView.loadUrl("javascript:OnClickTest()");

                //drudgeView.setVisibility(View.VISIBLE);

                reloadOpenXJsAds();

                progress.dismiss();

                isWriteDone = true;
            }
        }, delay);
    }

    public static void FireJS() {

        URL url;
        BufferedReader reader = null;

        try {
           // url = new URL("https://ancient-lake-9692.herokuapp.com/get?url=http://www.drudgereport.com");
            sTopHeadlines = "";
            url = new URL("http://www.imkdrapp.com/TopHeadlines.html");
            URLConnection con = url.openConnection();
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                sTopHeadlines = sTopHeadlines + line;
            }
           // Log.d("DRUDGE: ", sTopHeadlines.toString());

            sTopCenterHeadline = "";
            url = new URL("http://www.imkdrapp.com/TopCenterHeadline.html");
            con = url.openConnection();
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                sTopCenterHeadline = sTopCenterHeadline + line;
            }

            sDrudgeSiteLogo = "";
            url = new URL("http://www.imkdrapp.com/DrudgeSiteLogo.html");
            con = url.openConnection();
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                sDrudgeSiteLogo = sDrudgeSiteLogo + line;
            }

            sHeadlinesColumnOne = "";
            url = new URL("http://www.imkdrapp.com/HeadlinesColumn1.html");
            con = url.openConnection();
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                sHeadlinesColumnOne = sHeadlinesColumnOne + line;
            }

            sSourcesColumnOne = "";
            url = new URL("http://www.imkdrapp.com/SourcesColumn1.html");
            con = url.openConnection();
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                sSourcesColumnOne = sSourcesColumnOne + line;
            }

            sHeadlinesColumnTwo = "";
            url = new URL("http://www.imkdrapp.com/HeadlinesColumn2.html");
            con = url.openConnection();
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                sHeadlinesColumnTwo = sHeadlinesColumnTwo + line;
            }

            sSourcesColumnTwo = "";
            url = new URL("http://www.imkdrapp.com/SourcesColumn2.html");
            con = url.openConnection();
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                sSourcesColumnTwo = sSourcesColumnTwo + line;
            }

            sHeadlinesColumnThree = "";
            url = new URL("http://www.imkdrapp.com/HeadlinesColumn3.html");
            con = url.openConnection();
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                sHeadlinesColumnThree = sHeadlinesColumnThree + line;
            }

            sSourcesColumnThree = "";
            url = new URL("http://www.imkdrapp.com/SourcesColumn3.html");
            con = url.openConnection();
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                sSourcesColumnThree = sSourcesColumnThree + line;
            }



            //  Log.d("DRUDGE: ", s.toString());

           // JSONObject jsObj = new JSONObject(s);
          //  contents = jsObj.getString("contents");

       //     Log.d("DRUDGE: ", "MADE IT");
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }  catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }



        WriteHTML(0);

    }

    @TargetApi(16)
    protected void fixJellyBeanIssues() {

        try {
            webSettings.setAllowUniversalAccessFromFileURLs(true);
            webSettings.setAllowFileAccessFromFileURLs(true);
        } catch(NullPointerException e) {
            System.out.println(e.toString());
        }
    }

    public void ExitApp() {
        this.finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        if(currentApiVersion >= Build.VERSION_CODES.KITKAT && hasFocus)
        {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            event.startTracking();
            drudgeView.goBack();
            return true; //I have tried here true also
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
           // System.out.print("LONG PRESS");

            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Dialog_Alert);

            builder.setTitle("Exit Drudge");
            builder.setMessage("Are you sure?");

            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    // Do nothing but close the dialog

                    dialog.dismiss();
                    ExitApp();
                }

            });

            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Do nothing
                    dialog.dismiss();
                }
            });

            alert = builder.create();
            alert.show();

            return true;
        }
        return super.onKeyLongPress(keyCode, event);
    }

    private void startExternalWebView() {
        if (mExternalUrl != null) {
            Intent intent = new Intent(drudgeViewContext, ExternalWebview.class);
            intent.putExtra("newurl", mExternalUrl);
            startActivity(intent);
            mExternalUrl = null;
        }
    }

    private Runnable runLoadDisplayAd = new Runnable() {
        @Override
        public void run() {
            loadDisplayAd();
        }
    };

    private void loadDisplayAd() {
        if (mDisplayAdView != null) {
            if (!mDisplayAdReady) {
                mDisplayAdView.load();
            }

            return;
        }

        // Instantiate an ad view
        try {
            mDisplayAdView = new AdView(this,
                                        getResources().getString(R.string.ad_domain),
                                        getResources().getString(R.string.interstitial_display_adunit_id),
                                        AdConfiguration.AdUnitIdentifierType.AUID);
        }
        catch (Exception e) {
            Log.e(TAG, "AdView creation failed");
            return;
        }

        // Set auto display false, so interstitial ad is prefetched & can be used later.
        mDisplayAdView.setAutoDisplayOnLoad(false);

        // Set listener for ad life cycle events
        mDisplayAdView.addAdEventListener(m_displayAdEventListener);

        // Set close button position
        mDisplayAdView.interstitialProperties.pubClosePosition = InterstitialManager.OXMInterstitalClosePostition.TopRight;

        // Set custom close button image
        mDisplayAdView.interstitialProperties.pubCloseButtonImageView = ContextCompat.getDrawable(this, android.R.drawable.btn_dialog);

        // Set the opacity of the ad. Recommended 1.0f.
        mDisplayAdView.interstitialProperties.setPubBackGroundOpacity(1.0f);

        // Set user targeting parameters
        UserParameters userParameters = new UserParameters();
        userParameters.setUserGender(UserParameters.OXMGender.FEMALE);
        mDisplayAdView.setUserParameters(userParameters);

        // Request an ad
        mDisplayAdView.load();
    }

    private AdEventsListener m_displayAdEventListener = new AdEventsListener() {
        /*
        * Fires when an ad loads successfully
        */
        @Override
        public void adDidLoad(AdView adView) {
            Log.i(TAG, "Display - adDidLoad");
            mDisplayAdReady = true;
        }

        /*
         * Fires when an ad fails to load
         */
        @Override
        public void adDidFailToLoad(AdView adView, AdError adError) {
            Log.i(TAG, "Display - adDidFailToLoad");

            // Delay 15 seconds before attempting to load another ad
            mHandler.postDelayed(runLoadDisplayAd, 15000);
        }

        /*
         * Fires when an ad is displayed
         */
        @Override
        public void adDidDisplay(AdView adView) {
            Log.i(TAG, "Display - adDidDisplay");
        }

        /*
         * Fires when an ad completes
         */
        @Override
        public void adDidComplete(AdView adView) {
            Log.i(TAG, "Display - adDidComplete");
        }

        /*
         * Fires when an ad is clicked
         */
        @Override
        public void adWasClicked(AdView adView) {
            Log.i(TAG, "Display - adWasClicked");
        }

        /*
         * Fires when a click through view closes
         */
        @Override
        public void adClickThroughDidClose(AdView adView) {
            Log.i(TAG, "Display - adClickThroughDidClose");
        }

        /*
         * Fires when an interstitial ad closes
         */
        @Override
        public void adInterstitialDidClose(AdView adView) {
            Log.i(TAG, "Display - adInterstitialDidClose");
            startExternalWebView();

            // Optionally load another ad
            mDisplayAdView.load();
        }

        /*
         * Fires when an mraid expand is clicked
         */
        @Override
        public void adDidExpand(AdView adView) {
            Log.i(TAG, "Display - adDidExpand");
        }

        /*
         * Fires when an mraid collapse is clicked
         */
        @Override
        public void adDidCollapse(AdView adView) {
            Log.i(TAG, "Display - adDidCollapse");
        }
    };

    private Runnable runLoadVideoAd = new Runnable() {
        @Override
        public void run() {
            loadVideoAd();
        }
    };

    private void loadVideoAd() {
        if (mVideoAdView != null) {
            if (!mVideoAdReady) {
                mVideoAdView.load();
            }

            return;
        }

        // Instantiate an ad view
        try {
            mVideoAdView = new AdView(this, getResources().getString(R.string.interstitial_video_tag));
        }
        catch (Exception e) {
            Log.d(TAG, "AdView can not be created");
            return;
        }

        // Set listener for ad life cycle events
        mVideoAdView.addAdEventListener(m_videoAdEventListener);

        // Set video properties
        InterstitialVideoProperties properties = new InterstitialVideoProperties();

        // Configure auto preload to either Always, WIFI-only, or no auto preload
        properties.autoPreloadConfigs = VideoProperties.AutoVideoPreloadConfigs.AlwaysAutoPreload;

        // Time delay before close button appears
        properties.closeButtonDelayInSecs = 5;

        // Assign interstitial video properties to ad view
        mVideoAdView.setInterstitialVideoProperties(properties);

        // Set close button position
        mVideoAdView.interstitialProperties.pubClosePosition = InterstitialManager.OXMInterstitalClosePostition.TopRight;

        // Set custom close button image
        mVideoAdView.interstitialProperties.pubCloseButtonImageView = ContextCompat.getDrawable(this, android.R.drawable.btn_dialog);

        // Request an ad
        mVideoAdView.load();
    }

    private AdEventsListener m_videoAdEventListener = new AdEventsListener() {
        /*
        * Fires when an ad loads successfully
        */
        @Override
        public void adDidLoad(AdView adView) {
            Log.i(TAG, "Video - adDidLoad");
            mVideoAdReady = true;
        }

        /*
         * Fires when an ad fails to load
         */
        @Override
        public void adDidFailToLoad(AdView adView, AdError adError) {
            Log.i(TAG, "Video - adDidFailToLoad");

            // Delay 15 seconds before attempting to load another ad
            mHandler.postDelayed(runLoadVideoAd, 15000);
        }

        /*
         * Fires when an ad is displayed
         */
        @Override
        public void adDidDisplay(AdView adView) {
            Log.i(TAG, "Video - adDidDisplay");
        }

        /*
         * Fires when an ad completes
         */
        @Override
        public void adDidComplete(AdView adView) {
            Log.i(TAG, "Video - adDidComplete");
        }

        /*
         * Fires when an ad is clicked
         */
        @Override
        public void adWasClicked(AdView adView) {
            Log.i(TAG, "Video - adWasClicked");
        }

        /*
         * Fires when a click through view closes
         */
        @Override
        public void adClickThroughDidClose(AdView adView) {
            Log.i(TAG, "Video - adClickThroughDidClose");
        }

        /*
         * Fires when an interstitial ad closes
         */
        @Override
        public void adInterstitialDidClose(AdView adView) {
            Log.i(TAG, "Video - adInterstitialDidClose");
            startExternalWebView();

            // Optionally load another ad
            mVideoAdView.load();
        }

        /*
         * Fires when an mraid expand is clicked
         */
        @Override
        public void adDidExpand(AdView adView) {
            Log.i(TAG, "Video - adDidExpand");
        }

        /*
         * Fires when an mraid collapse is clicked
         */
        @Override
        public void adDidCollapse(AdView adView) {
            Log.i(TAG, "Video - adDidCollapse");
        }
    };

    public void showInterstialAd() {
        if (mVideoAdReady) {
            mVideoAdView.showVideoAsInterstitial();
            mVideoAdReady = false;
        } else if (mDisplayAdReady) {
            mDisplayAdView.showAsInterstitialFromRoot();
            mDisplayAdReady = false;
        }
    }

    public AdvertisingData getAdvertisingData() {
        SharedPreferences prefs = this.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String advertisingId = prefs.getString(PREF_ADVERTISING_ID, null);
        boolean isLimitAdTrackingEnabled = prefs.getBoolean(PREF_LIMIT_AD_TRACKING_ENABLED, false);
        if (advertisingId == null) {
            GetAdvertisingInfo.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            return null;
        }

        AdvertisingData data = new AdvertisingData();
        data.AdvertisingId = advertisingId;
        data.IsLimitAdTrackingEnabled = isLimitAdTrackingEnabled;
        return data;
    }

    public Location getLastLocation() {
        return LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    }

    AsyncTask<Void, Void, AdvertisingIdClient.Info> GetAdvertisingInfo = new AsyncTask<Void, Void, AdvertisingIdClient.Info>() {

        @Override
        protected AdvertisingIdClient.Info doInBackground(Void... params) {
            AdvertisingIdClient.Info advertisingInfo = null;
            try {
                advertisingInfo = AdvertisingIdClient.getAdvertisingIdInfo(getApplicationContext());
            }
            catch (Exception e) {
                e.printStackTrace();
            }

            return advertisingInfo;
        }

        @Override
        protected void onPostExecute(AdvertisingIdClient.Info info) {
            if (info == null) {
                return;
            }

            SharedPreferences prefs = MainActivity.this.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
            prefs.edit().putString(PREF_ADVERTISING_ID, info.getId()).apply();
            prefs.edit().putBoolean(PREF_LIMIT_AD_TRACKING_ENABLED, info.isLimitAdTrackingEnabled()).apply();
        }
    };

    private static void reloadOpenXJsAds() {
        drudgeView.postDelayed(new Runnable() {
            @Override
            public void run() {
                drudgeView.loadUrl("javascript:setupOpenXAds();");
            }
        }, 1000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mDisplayAdView != null) {
            mDisplayAdView.destroy();
        }
        if (mVideoAdView != null) {
            mVideoAdView.destroy();
        }
    }
}

